const { twitchChannels, twitchToken, twitchId, alertChannel, alertRole } = require('../../config.json');
const { EmbedBuilder } = require('discord.js');
const get = require('../util/getJson.js');
const https = require('https');
const fs = require('fs');
const path = require('path');

module.exports = {
    name: 'twitchChecker',
    async start(client) {
        let alerted = null;
        let wentOffline = null;
        const file = path.join(__dirname, '../files/tmp/twitchAlert.txt');
        const errorMargin = 600000; // margin after how long offline can be alerted again

        // send the alert
        async function sendTwitchAlert(game, title, username, thumbnail_url, viewers) {
            setAlert(true);
            client.channels.fetch(alertChannel).then(channel => {
                thumbnail_url = thumbnail_url.replace('{width}', '500').replace('{height}', '281')
                //console.log(`🔴 He's **live**!\n${title}\nCurrently playing ${game}`)
                
                const liveEmbed = new EmbedBuilder()
                    .setColor('#9147ff')
                    .setTitle(`🔴 ${username} - ${title}`)
                    .setImage(`${thumbnail_url}?rnd=${Date.now()}`)
                    .setURL(`https://twitch.tv/${username}`)
                    .setDescription(`Streaming ${game} to ${viewers} people`)
                    .setTimestamp(Date.now())         

                channel.send({
                    content: `<@&${alertRole}>`, 
                    embeds: [liveEmbed]
                })
            })
        }

        // get twitch auth token
        async function getAuth() {
            let params = {
                client_id: twitchId, 
                client_secret: twitchToken,
                grant_type: "client_credentials"
            }
            params = new URLSearchParams(params).toString(); 

            const options = {
                hostname: 'id.twitch.tv',
                port: 443,
                path: '/oauth2/token',
                method: 'POST'
            }

            return new Promise((resolve, reject) => {
                const req = https.request(options, res => {
                    let rawData = '';
                    res.on('data', d => {
                        rawData += d;
                    })
        
                    res.on('close', () => {
                        try {
                            const parsedData = JSON.parse(rawData);
                            resolve(parsedData);
                        } catch (error) {
                            reject(error.message);
                        }
                    })
                }).on('error', error => {
                    reject(error.message);
                })    
                
                req.write(params)
                req.end()
            })
        }

        // check stream data
        function check(channel, token) {
            return new Promise((resolve, reject) => {
                const headers = {
                    'Client-Id': twitchId,
                    'Accept': 'application/vnd.twitchtv.v5+json',
                    'Authorization': `Bearer ${token}`
                }

                let params = new URLSearchParams(); 
                channel.forEach( name =>  params.append('user_login', name))
                params = params.toString()
                
                get('api.twitch.tv', `/helix/streams?${params}`, headers)
                .then( res => {
                    resolve(res)
                })
                .catch( err => { 
                    console.error(err);
                })
            })
        }

        // check if alert has been sent 
        async function checkAlert() {
            if (alerted === true) return true;
            
            return new Promise((resolve, reject) => {
                if (!alerted) { // not alerted, check file if that's true
                    if (fs.existsSync(file)) {
                        fs.readFile(file, 'utf8' , (err, data) => {
                            if (err) { 
                                console.log(`Error reading alert status file: ${err}`);
                                reject(err);
                            } else {
                                resolve(JSON.parse(data));
                            }
                        })
                    } else { // file doesn't exist, set current alert status to false
                        setAlert(false).then(reject('File not found'));
                    }
                }
            })
        }

        // set alert var and file to given boolean
        async function setAlert(setAlert) {
            alerted = setAlert;

            return new Promise((resolve, reject) => {
                fs.writeFile(file, JSON.stringify(alerted), err => {
                    if (err) {
                        console.error(`Error writing alert status to disk: ${err}`)
                        reject(err);
                    } else resolve()
                })
            })
        }

        // decides if alert needs to be sent and acts on it
        async function decideAlert(data) { 
            let live = data.length > 0;
            await checkAlert()
            .then( alerted => {
                if (live && !alerted) { // live, not alerted & over error margin
                if (wentOffline === null || (wentOffline !== null && (Date.now() - wentOffline) > errorMargin))
                    sendTwitchAlert(
                        data[0].game_name, 
                        data[0].title, 
                        data[0].user_name, 
                        data[0].thumbnail_url,
                        data[0].viewer_count
                        );
                }
                else if (!live) {
                    if (alerted) { // not live, alerted
                        setAlert(false);
                        wentOffline = Date.now();
                    } 
                }
            })
            .catch( err => console.error(`Couldn't check alert: ${err}`));
        }

        // set the interval at which this needs to be run and do the logic
        setInterval( ()=> {
            getAuth()
            .then( res => {
                check( twitchChannels, res.access_token )
                .then( res => { decideAlert(res.data) })
                .catch( err => console.error(err) )
            })
            .catch( err => console.error(err) )
        }, 15000);
    },
}