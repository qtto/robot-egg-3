const updateDb = require('../util/strikes/update-db.js');

module.exports = {
    name: 'updateStrikes',
    async start(client) {
        // set the interval at which this needs to be run and do the logic
        setInterval( ()=> {
            updateDb()
            .then(res => {
                if(res > 0) console.log(`Updated ${res} strikes.`);
            })
            .catch(err => console.error(`Error in updateStrikes: ${err}`))
        }, 600000);
    },
}