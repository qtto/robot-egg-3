const { SlashCommandBuilder } = require('discord.js');
const download = require('../util/getJson.js');
const fs = require('fs')
const path = require('path')

module.exports = {
	data: new SlashCommandBuilder()
		.setName('cat')
		.setDescription('Force an innocent cat to say something')
		.addStringOption(option => option.setRequired(true).setName('input').setDescription('Enter some text'))
		.addStringOption(option => option.setName('tag').setDescription('Enter a tag')),
	async execute(interaction) {
		let string = interaction.options.getString('input');
		const tag = interaction.options.getString('tag');
		const charWidthList = {"0":54,"1":38,"2":50,"3":53,"4":50,"5":54,"6":54,"7":39,"8":53,"9":54," ":18,"!":27,"\"":37,"#":63,"$":55,"%":69,"&":58,"'":19,"(":31,")":31,"*":28,"+":53,",":17,"-":29,".":18,"/":40,":":20,";":20,"<":53,"=":53,">":53,"?":52,"@":77,"A":51,"B":55,"C":55,"D":55,"E":42,"F":40,"G":55,"H":56,"I":29,"J":33,"K":54,"L":38,"M":72,"N":54,"O":55,"P":50,"Q":55,"R":54,"S":52,"T":46,"U":55,"V":52,"W":81,"X":48,"Y":47,"Z":40,"":28,"\\":40,"]":28,"^":48,"_":55,"`":33,"a":50,"b":52,"c":49,"d":52,"e":51,"f":29,"g":52,"h":52,"i":27,"j":28,"k":48,"l":27,"m":77,"n":52,"o":51,"p":52,"q":52,"r":36,"s":47,"t":30,"u":52,"v":44,"w":67,"x":43,"y":45,"z":35,"{":37,"|":27,"}":37,"~":52};
		const widthModifier = 15/55 // this O is 15px instead of 55, all other letters size down by the same % presumably. 
		const maxTextWidth = 500; // leave some space on the sides 8)
		const imageWidth = 512; // set a width for the cat image to be displayed
		const newLine = '\n'; // character to make line break

		await interaction.deferReply();

		if(string.length > 512) {
			interaction.editReply({ content: 'Calm down, that\'s too much for the cat to say, it can barely talk', ephemeral: true });
			return;
		};

		const file = path.join(__dirname, '../files/tmp/catlist.txt')
		let exists = false;
		let update = (Math.floor(Math.random() * 100) < 3) // percentage chance to update the list (maybe the api gets updated???)
		let cats = null // variable to hold the cats so we don't need to read the file from disk all the time

		try {
			if (cats != null || fs.existsSync(file)) { // don't need to check if they're in memory
			  	exists = true;
			}
		} catch(err) {
			exists = false;
		}
		
		// get the catlist and save it to file + in memory
		if (!exists || update) {
			await download('cataas.com', '/api/cats?tags=&limit=1000')
                .then(response => { 
					cats = response;
					cats = cats.filter(x => !x.tags.includes('gif')); // filter out gifs

                    fs.writeFile(file, JSON.stringify(cats), err => {
						if (err) console.log(`Error writing cats to disk: ${err}`);
				    })

				   sendCat(cats);
                })
                .catch(err => { 
                    console.log(`Couldn't get cats (${err}).`);
                })
		} else if (cats === null) {
			await readCat(cats)
			.then(catList => {
				sendCat(catList);
			})
			.catch(err => {
				console.log(`Couldn't read cats (${err}).`)
			})
		} else {
			sendCat(cats);
		}

		async function readCat(cats) {
			if (cats === null) { // presumably on disk but not in memory
				return new Promise((resolve, reject) => {
					try {
						fs.readFile(file, 'utf8' , (err, data) => {
							if (err) { 
								console.log(`Error reading cat file: ${err}`);
								reject(err);
							} else resolve(JSON.parse(data));
						})
					} catch { 
						console.log(`Error reading cat file: ${err}`);
						reject(err);
					}
				});
			}
		}

		async function sendCat(cats) {			
			if (cats != null ) { // we go if we have the cats
				if(tag) {
					cats = cats.filter(x => x.tags.includes(tag));
					if (cats.length === 0) {
						await interaction.editReply({
							content: `No cats found with ${tag}`, ephemeral: true 
						}).catch(err => { console.error(err) })
	
						return
					}
				}
				const randomCat = cats[Math.floor(Math.random() * cats.length)];
				const id = randomCat._id;

				string = string.replace(/[^\w\s\.\,\?\!\@\'\$\%\^\&\*\(\)\-\<\>\:\;\#"]/gi, ''); // cut out special characters except those i deem worthy (yeah i suck at regex)
				if (string.length < 1) {
					await interaction.editReply({
						content: 'Invalid text for cat, give letters', ephemeral: true 
					}).catch(err => { console.error(err) })

					return
				}

				function trueWidth(letter) { 
					// calculate letter width in px
					try {
						let width = charWidthList[letter];
						if (width.isNaN()) return 16;
						return Math.ceil(width*widthModifier)+1 // 1 for whitespace next to each letter
					} catch { 
						return 16; //default if we cant find the character
					}
				}

				function stringWidth(string) { 
					// calculate the width of a given string in px
					let width = 0;
					for (const char of string) {
						width += trueWidth(char);
					}
					return width;
				}


				// this whole bit checks if the text fits, else it makes it wrap
				if (stringWidth(string) > maxTextWidth) {
					let strArr = string.split(' ');
					let fittingText = [];
					let sentences = 0; // counter for fittingText array

					let i = 0;
					while (i < strArr.length) { 
						if (fittingText?.[sentences] === undefined) fittingText.push('') // make sure there's always a string at current pos so it doesnt break on .length
						
						//first: if it can fit in with the last sentence.
						if ((stringWidth(strArr[i]) + stringWidth(fittingText[sentences] + ' ')) < maxTextWidth) {
							// if there's text in the sentence already, also include a space
							fittingText[sentences].length > 0 ? fittingText[sentences] += ` ${strArr[i]}` : fittingText[sentences] += strArr[i] 
						} else if (stringWidth(strArr[i]) < maxTextWidth) {
							//if it doesn't fit in with the last sentence, make a new one
							sentences++;
							fittingText[sentences] = newLine + strArr[i];
						} else { 
							// the text doesnt fit in a sentence at all, even though it's split over spaces already. Fill up last sentence and go again
							let currentLength = stringWidth(fittingText[sentences] + ' ');
							let toAdd = maxTextWidth - currentLength;
							let fittingSubStr = '';
							while (stringWidth(fittingSubStr) < toAdd) {
								fittingSubStr += strArr[i].substr(0, 1);
								strArr[i] = strArr[i].substr(1,);
							}
							fittingText[sentences].length > 0 ? fittingText[sentences] += ` ${fittingSubStr}` : fittingText[sentences] += fittingSubStr
							// make the current word shorter and go agane at the same index
							i--; // redo this iteration because we changed the string at this index
						}

						i++;
					}

					string = fittingText.join(' ');
				}

				// encode special characters 
				string = encodeURIComponent(string);

				await interaction.editReply({ 
				 	files: [{
				 		attachment: `https://cataas.com/cat/${id}/says/${string}?width=${imageWidth}`,
				 		name: 'cat.png'
				 	}]
				}).catch(err => { console.error(err) })

			} else { // how this could be null? idk but we can't force a cat to talk if we dont have a cat
				await interaction.editReply({
					content: 'Something went wrong with the cats, sorry brother', ephemeral: true 
				}).catch(err => { console.error(err) })
			}
		}
	}
};