const { SlashCommandBuilder } = require('@discordjs/builders');
const download = require('../util/getJson.js');
const Jimp = require('jimp');


module.exports = {
	data: new SlashCommandBuilder()
		.setName('dog')
		.setDescription('Force a guilty dog to say something')
		.addStringOption(option => option.setRequired(true).setName('input').setDescription('Enter some text')),
	async execute(interaction) {
		let string = interaction.options.getString('input');

		await interaction.deferReply();

		if(string.length > 512) {
			interaction.editReply({ content: 'Calm down, that\'s too much for the dog to say, even though it speaks very well', ephemeral: true });
			return;
		};
		
	
		async function getDog() {
			return new Promise((resolve, reject) => {
				download('dog.ceo', '/api/breeds/image/random')
				.then(response => { 
					let url = response.message;
					resolve(url)
				})
				.catch(err => { 
					reject(`Couldn't get dogs (${err}).`);
				})
			})
		}


		async function makeDog(url) {
			const path = `${__dirname}/../files/impact.fnt`;

			return new Promise((resolve, reject) => {
				Jimp.read(url)
				.then(dog => {
					Jimp.loadFont(path)
					.then(font => {
						dog = dog.resize(512, Jimp.AUTO)

						let maxWidth = dog.bitmap.width;
						let maxHeight = dog.bitmap.height;

						dog = dog.print(
							font, 0, -25,
							{
								text: string,
								alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
								alignmentY: Jimp.VERTICAL_ALIGN_BOTTOM
							}, 
							maxWidth, maxHeight
						)
						
						dog.getBuffer(Jimp.AUTO, (err, buffer) => {
							if (err) reject(err)
							resolve(buffer);
						});
					})
					.catch( err => { reject(`Couldn't let dog talk: ${err}`) })
				})
				.catch( err => { reject(`Couldn't load dog: ${err}`) })
			})
		}

		async function sendDog(dogBuffer) {
			await interaction.editReply({
				files: [{
					attachment: dogBuffer
				}]
			}).catch(err => { console.error(err) })
		}

		getDog()
		.then( res => {
			return makeDog(res)
			.catch(err => {console.log(err);interaction.editReply({content: err, ephemeral: true})})
		})	
		.then( res => {
			sendDog(res)
		})
		.catch(err => interaction.editReply({content: err, ephemeral: true}))
	}
};