const { SlashCommandBuilder } = require('@discordjs/builders');
const checkPerms = require('../util/check-perms.js');
const sendMail = require('../util/send-mail.js');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('mail')
		.setDescription('DM a user.')
		.addUserOption(option => option.setRequired(true).setName('user').setDescription('Provide user to message'))
		.addStringOption(option => option.setRequired(true).setName('message').setDescription('Enter message')),
	async execute(interaction) {
		const user = interaction.options.getUser('user');
		const message = interaction.options.getString('message');

		if (!checkPerms('mod', interaction.user.id, interaction.client, interaction.member.roles)) {
            await interaction.reply({content: "Invalid permissions. Try grinding more.", ephemeral: true})
            return
        }

		await interaction.reply({ content: 'Sending message...' })
		await sendMail(user, message)
		.then(res => {
			interaction.editReply({ content: res })
		})
		.catch(err => { 
			console.error(err);
			interaction.editReply({ content: err });
			return;
		 })
	},
};
