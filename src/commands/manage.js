const { ActionRowBuilder, ButtonBuilder, ButtonStyle, StringSelectMenuBuilder, userMention, roleMention } = require('discord.js');
const { SlashCommandBuilder } = require('@discordjs/builders');
const { toggleableRoles } = require('../../config.json');
const checkPerms = require('../util/check-perms.js');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('manage')
        .setDescription('Manage a user\'s roles.')
        .addUserOption(option => option.setRequired(true).setName('user').setDescription('Provide user to manage')),

    async execute(interaction) {
        const user = interaction.options.getMember('user');
        const client = interaction.client;

        async function getMember() {
            return new Promise((resolve, reject) => {
                user.fetch()
                .then(member => { resolve(member)})
                .catch(err => { reject(err) });
            })
        }

        if (!checkPerms('manager', interaction.user.id, interaction.client, interaction.member.roles)) {
            await interaction.reply({content: "Invalid permissions. Try grinding more.", ephemeral: true})
            return
        }

        await interaction.reply({
            content: '*Fetching roles...*'
        });

        let addable = [];
        let removable = [];
        await getMember()
            .then(member => {
                let roles = member._roles;
                for (const roleName in toggleableRoles) {
                    if (roles.includes(toggleableRoles[roleName])) {
                        removable.push({
                            label: roleName,
                            value: toggleableRoles[roleName]
                        })
                    }
                    else {
                        addable.push({
                            label: roleName,
                            value: toggleableRoles[roleName]
                        })
                    }
                }
            })
            .catch( err => console.error(err) );


        
        const addMenu = new ActionRowBuilder()
        .addComponents(
            new StringSelectMenuBuilder()
            .setCustomId('add')
            .setPlaceholder('Role to add')
            .addOptions(addable))
        
        const removeMenu = new ActionRowBuilder()
        .addComponents(
            new StringSelectMenuBuilder()
            .setCustomId('remove')
            .setPlaceholder('Role to remove')
            .addOptions(removable))

        const singleCancelButton = new ButtonBuilder()
            .setCustomId('cancel')
            .setLabel('Cancel')
            .setStyle(ButtonStyle.Secondary)

        // single cancel button
        const cancelButton = new ActionRowBuilder()
        .addComponents(
            singleCancelButton
        );

        // confirm + cancel button
        const confirmButtons = new ActionRowBuilder()
        .addComponents(
            new ButtonBuilder()
                .setCustomId('ok')
                .setLabel('Toggle')
                .setStyle(ButtonStyle.Primary),
            singleCancelButton
        );

        // success
        const finishedButtons = new ActionRowBuilder()
        .addComponents(
            new ButtonBuilder()
                .setCustomId('ok')
                .setLabel('Toggle')
                .setStyle(ButtonStyle.Primary)
                .setDisabled(true)
        );

        // menu to select role
        // create first prompt: select role
        let comps = [];
        if (addable.length > 0) comps.push(addMenu);
        if (removable.length > 0) comps.push(removeMenu);
        comps.push(cancelButton);

        await interaction.editReply({
            content: `Managing roles for ${user}.`,
            components: comps
        });

        // collect data sent from components on the message.
        const message = await interaction.fetchReply();
        const filter = i => i.user.id === interaction.user.id;
        const collector = message.createMessageComponentCollector({filter, time: 900000 });
        
        collector.on('collect', async i => {                
            const value = i.customId;   
            let content = '';        
            let comps = [];     

            // collect data from tier menu
            if (value === 'add') {
                const role = i.values[0];

                await getMember()
                .then(member => {
                    member.roles.add(role);
                })

                content = `${userMention(i.user.id)} added ${roleMention(role)} to ${userMention(user.id)}`
                
            }

            else if (value === 'remove') {
                const role = i.values[0];

                await getMember()
                .then(member => {
                    member.roles.remove(role);
                })

                content = `${userMention(i.user.id)} removed ${roleMention(role)} from ${userMention(user.id)}`
            }

            
            else if (value === 'cancel') {
                content = `${userMention(i.user.id)} cancelled updating roles for ${userMention(user.id)}`;
            }

            else {console.log(value); content = 'Something went wrong!';}
            // collect data from all different buttons
                    
            await i.update({ content, components: comps }).catch(error => console.error(error));

        });

        collector.on('end', async i => {
            content = `${message.content} \n\n*Interaction finished:* ${collector.endReason}`
            await interaction.editReply({ content, components: [] });
        });
    }
};
