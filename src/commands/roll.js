const { SlashCommandBuilder } = require('@discordjs/builders');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('roll')
		.setDescription('Roll a die.')
		.addNumberOption(option => option.setName('min').setDescription('Min roll (default 1)'))
		.addNumberOption(option => option.setName('max').setDescription('Max roll (default 6)')),
	async execute(interaction) {
		let min = interaction.options.getNumber('min');
		let max = interaction.options.getNumber('max');
		if (!min) min = 1;
		if (!max) max = 6;

		if( min > max ) 
			await interaction.reply({ content: 'Make sure the minimum roll is smaller than the maximum!', ephemeral: true })
		else {
			roll = Math.floor(Math.random() * (max - min + 1) + min);
			await interaction.reply({ content: `You rolled ${roll}.` })
		}
	},
};
