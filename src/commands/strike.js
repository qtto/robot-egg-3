const { ActionRowBuilder, ButtonBuilder, ButtonStyle, StringSelectMenuBuilder, userMention } = require('discord.js');
const { SlashCommandBuilder } = require('@discordjs/builders');
const sendStrike = require('../util/strikes/send-strike.js');
const viewStrike = require('../util/strikes/view-strike.js');
const removeStrike = require('../util/strikes/remove-strike.js');
const checkPerms = require('../util/check-perms.js');
const sendMail = require('../util/send-mail.js');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('strike')
        .setDescription('Commands related to strikes.')
        //.setDefaultPermission(false)
        .addSubcommand(subcommand =>
            subcommand
                .setName('add')
                .setDescription('Strikes a user')
                .addUserOption(option => option.setRequired(true).setName('user').setDescription('Provide user to strike'))
                .addStringOption(option => option.setRequired(true).setName('reason').setDescription('Enter a reason for the strike'))
                .addStringOption(option => option.setName('notes').setDescription('Notes (optional)')))
        .addSubcommand(subcommand =>
            subcommand
                .setName('view')
                .setDescription('Shows a user\'s strikes')
                .addUserOption(option => option.setRequired(true).setName('user').setDescription('Provide user to inspect')))
        .addSubcommand(subcommand =>
            subcommand
                .setName('remove')
                .setDescription('Removes a strike from a user')
                .addUserOption(option => option.setRequired(true).setName('user').setDescription('User to remove strike from'))),

    async execute(interaction) {
        const user = interaction.options.getUser('user');
        
        // Add strikes
        if (interaction.options.getSubcommand() === 'add') {
            const reason = interaction.options.getString('reason');
            const notes = interaction.options.getString('notes');

            if (!checkPerms('mod', interaction.user.id, interaction.client, interaction.member.roles)) {
                await interaction.reply({content: "Invalid permissions. Try grinding more.", ephemeral: true})
                return
            }

            // Check if strike message is valid length.
            if ( reason.length > 1500 || (notes && notes.length + reason.length > 1500 ) ) { 
                await interaction.reply({content: 'Message too long.'})
            }

            // menu to select strike tier
            const selectMenu = new ActionRowBuilder()
                .addComponents(
                    new StringSelectMenuBuilder()
                        .setCustomId('color')
                        .setPlaceholder('Select strike tier')
                        .addOptions([
                            { label: 'Green', description: '1 point', value: 'green', },
                            { label: 'Yellow', description: '2 points', value: 'yellow', },
                            { label: 'Orange', description: '4 points', value: 'orange', },
                            { label: 'Red', description: '8 points', value: 'red', },
                            { label: 'Crimson', description: '12 points', value: 'crimson', },
                        ]),
                );

                // prepare single buttons
                const singleCancelButton = new ButtonBuilder()
                    .setCustomId('cancel')
                    .setLabel('Cancel')
                    .setStyle(ButtonStyle.Danger)

                // cancel button row
                const cancelButton = new ActionRowBuilder()
                .addComponents(
                    singleCancelButton
                );

                // confirm + cancel button
                const confirmButtons = new ActionRowBuilder()
                .addComponents(
                    new ButtonBuilder()
                        .setCustomId('ok')
                        .setLabel('Strike')
                        .setStyle(ButtonStyle.Primary),
                    singleCancelButton
                );

                // success
                const finishedButtons = new ActionRowBuilder()
                .addComponents(
                    new ButtonBuilder()
                        .setCustomId('ok')
                        .setLabel('Strike')
                        .setStyle(ButtonStyle.Success)
                        .setDisabled(true)
                );
            
            // create first prompt: select tier list
            let content = `\`User:\` ${userMention(user.id)}\n\`Reason:\` ${reason}`;
            if (notes) {content += `\n\`Notes:\` ${notes}`};

            await interaction.reply({ 
                content: content,
                components: [selectMenu, cancelButton]
            });

            // collect data sent from components on the message.
            const message = await interaction.fetchReply();
            const filter = i => i.user.id === interaction.user.id;
            const collector = message.createMessageComponentCollector({filter, time: 900000 });
            let strikeColor = ''; // save strike color for user msg
            let strikePoints = ''; // same for total strike points
            let strike_tier = null; // we get this later too

            collector.on('collect', async i => {
                const value = i.customId;

                // collect data from tier menu
                if (value === 'color') {
                    strikeColor = i.values[0];
                    await i.update({
                        content: message.content + '\n\`Tier:\` ' + strikeColor + '\n', 
                        components: [confirmButtons]
                    }).catch(error => console.error(error));

                    // convert tier text to number
                    const tiers = {'green': 1, 'yellow': 2, 'orange': 3, 'red': 4, 'crimson': 5};
                    strike_tier = tiers[strikeColor];
                }

                // collect data from all different buttons
                else {
                    comps = [];

                    switch (value) {
                        // if ok button is pressed, send the strike.
                        case 'ok':
                            await sendStrike(user.id, message.author.id, reason, notes, strike_tier)
                            .then(response => { 
                                strikePoints = response.user_points;
                                content = `**${userMention(i.user.id)} created a strike:**\n${message.content}`;
                                content += `\n\nUser has ${response.user_points} points. Mute for ${response.mute}`;
                                if (response.user_points >= 12) { content += " **This user should be banned.**"}

                                let userMsg = `Hi ${user},\n\nYou received a ${strikeColor} strike in Eggserver with the following reason:\n> ${reason}.\n\nYou now have ${strikePoints}/12 strike points. DM the mods for more information.`;
                                sendMail(user, userMsg).catch(err => { console.log(err) });

                                comps = [finishedButtons];
                            })
                            .catch(error => { 
                                content = `__**${userMention(i.user.id)}, there was an error creating this strike:**__\n\n${error}`;
                            })
                            break;
                            // cancel button cancels strike.
                        case 'cancel':
                            content = `${userMention(i.user.id)} cancelled strike for ${userMention(user.id)}`;
                            break;
                        default:
                            content = 'Ruh roh, something went wrong!';
                            break;
                        }
                        
                    await i.update({ content, components: comps }).catch(error => console.error(error));
                }
            });

            collector.on('end', async i => {
                content = `${message.content} \n\n*Interaction finished:* ${collector.endReason}`
                await i.editReply({ content, components: [] }).catch(error => console.error(error));
            });
        }

        // View strikes
        if (interaction.options.getSubcommand() === 'view') {
            if ((!checkPerms('mod', interaction.user.id, interaction.client, interaction.member.roles) && interaction.user.id != user.id)) {
                await interaction.reply({content: "Invalid permissions. Try grinding more.", ephemeral: true})
                return
            }

            await interaction.reply({ 
                content: '*Fetching strikes...*',
                ephemeral: interaction.user.id === user.id
            });

            let content = '';
            await viewStrike(user.id)
                .then(response => { 
                    content = `${userMention(user.id)} has ${response.strike_points} strike points.`;
                    response.strikes.forEach((item, i) => {
                        let strike = `Strike ${i} (${item.tier}) - ${item.reason}`;
                        if (item.removed) content += '```haskell\n#REMOVED\n' + strike + '```';
                        else if (item.expired) content += '```haskell\n#EXPIRED\n' + strike + '```';
                        else content += '```haskell\n' + strike + '\n```';
                    });
                })
                .catch(error => { 
                    content = `Couldn't get strikes for this user (${error}).`;
                })
            
            await interaction.editReply(content);
        }

        // Remove strikes
        if (interaction.options.getSubcommand() === 'remove') {
            if (!checkPerms('mod', interaction.user.id, interaction.client, interaction.member.roles)) {
                await interaction.reply({content: "Invalid permissions. Try grinding more.", ephemeral: true})
                return
            }

            await interaction.reply({
                content: '*Fetching strikes...*'
            });

            let content = '';
            let strikes = [];
            
            await viewStrike(user.id)
                .then(response => { 
                    content = `${userMention(user.id)} has ${response.strike_points} strike points.`;
                    response.strikes.forEach((item, i) => {
                        let strike = `Strike ${i} (${item.tier}) - ${item.reason}`
                        if (!item.removed && !item.expired) {
                            content += '```haskell\n' + strike + '\n```';
                            strikes.push({'index': i, 'id': item.id, 'tier': item.tier, 'reason': item.reason})
                        }
                    });
                })
                .catch(error => { 
                    content = `Couldn't get strikes for this user (${error}).`;
                })

            if(strikes.length < 1) {
                await interaction.editReply({
                    content: content
                });
                return
            }

            strikes.forEach((item, i) => {
                // discord set a max of 25 items per list, so only add 25.
                if (i <= 25) {
                    strikes[i] = {
                        label: `${item.reason} (${item.tier})`,
                        description: `ID: ${item.id}`,
                        value: item.id.toString()
                    }
                }
            })

            // menu to select strike tier
            const selectMenu = new ActionRowBuilder()
                .addComponents(
                    new StringSelectMenuBuilder()
                .setCustomId('strike')
                .setPlaceholder('Strike to remove')
                .addOptions(strikes))

            const singleCancelButton = new ButtonBuilder()
                .setCustomId('cancel')
                .setLabel('Cancel')
                .setStyle(ButtonStyle.Secondary)

            // single cancel button
            const cancelButton = new ActionRowBuilder()
                .addComponents(
                    singleCancelButton
                );

            // confirm + cancel button
            const confirmButtons = new ActionRowBuilder()
                .addComponents(
                    new ButtonBuilder()
                        .setCustomId('ok')
                        .setLabel('Remove')
                        .setStyle(ButtonStyle.Danger),
                    singleCancelButton
                );

            // success
            const finishedButtons = new ActionRowBuilder()
                .addComponents(
                    new ButtonBuilder()
                        .setCustomId('ok')
                        .setLabel('Remove')
                        .setStyle(ButtonStyle.Danger)
                        .setDisabled(true)
                );
        
            // create first prompt: select strike
            await interaction.editReply({
                content: content,
                components: [selectMenu, cancelButton]
            });

            // collect data sent from components on the message.
            const message = await interaction.fetchReply();
            const filter = i => i.user.id === interaction.user.id;
            const collector = message.createMessageComponentCollector({filter, time: 900000 });
            // data from strike that is being removed
            let strikeData = ''; // [ { label: reason (tier), description: ID id, value: id, } ] 
            
            collector.on('collect', async i => {                
                const value = i.customId;                

                // collect data from tier menu
                if (value === 'strike') {
                    const id = i.values[0];
                    strikes.forEach((strike, i) => { if (strike.value == id) { strikeData = strike }});
                    await i.update({
                        content: `Removing strike \`#${id}\`\n\n${strikeData.label}`,
                        components: [confirmButtons]
                    }).catch(error => console.error(error));
                }

                // collect data from all different buttons
                else {
                    comps = [];

                    switch (value) {
                        case 'ok':
                            await removeStrike(i.user.id, strikeData.value)
                            .then(response => { 
                                content = `${userMention(i.user.id)} removed strike \`#${strikeData.value}\` from ${userMention(user.id)}\n\n> ${strikeData.label}`,
                                comps = [finishedButtons];
                            })
                            .catch(error => { 
                                content = `__**${userMention(i.user.id)}, there was an error removing this strike:**__\n\n${error}`;
                            })
                            break;
                        case 'cancel':
                            content = `${userMention(i.user.id)} cancelled removing strike for ${userMention(user.id)}`;
                            break;
                        default:
                            content = 'Something went wrong!';
                            break;
                        }
                        
                    await i.update({ content, components: comps }).catch(error => console.error(error));
                }
            });

            collector.on('end', async i => {
                content = `${message.content} \n\n*Interaction finished:* ${collector.endReason}`
                await interaction.editReply({ content, components: [] });
            });
        }
    }
};
