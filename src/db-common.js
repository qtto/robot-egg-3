const sqlite3 = require('sqlite3')

const dbFile = `${__dirname}/files/app.db`;
exports.dbFile = dbFile;

const connectDb = () => {
    return new sqlite3.Database(dbFile, (err) => {
        if (err) {
            console.error(err.message);
            reject(`Couldn\'t connect to database (${err})`);
        }
    })
}
exports.connectDb = connectDb;

const closeDb = (db) => {
    db.close((err) => {
        if (err) {
            console.error(err.message);
            reject(`Couldn\'t close database (${err})`);
        }
    });
}
exports.closeDb = closeDb;