const fs = require('fs');
const path = require('path');
const { REST } = require('@discordjs/rest');
const { Routes } = require('discord-api-types/v9');
const { clientId, testGuildId, mainGuildId, token } = require('../config.json');

const commands = [];
const commandDir = path.resolve(__dirname, './commands');
const commandFiles = fs.readdirSync(commandDir).filter(file => file.endsWith('.js'));

for (const file of commandFiles) {
	const command = require(`./commands/${file}`);
	commands.push(command.data.toJSON());
}

const rest = new REST({ version: '9' }).setToken(token);

(async () => {
	try {
		// register on testserver
		 await rest.put(
		 	Routes.applicationGuildCommands(clientId, testGuildId),
		 	{ body: commands },
		 );

		// register globally
		await rest.put(
			Routes.applicationCommands(clientId),
			{ body: commands },
		);

		console.log('Successfully registered application commands.');
	} catch (error) {
		console.error(error);
	}
})();