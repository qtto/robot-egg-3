const sendModmail = require('../util/modmail.js');

module.exports = {
    name: 'messageCreate', 
    async execute(message){
        // message channel = author dm channel -> it's a DM
        if (message.author.dmChannel && message.channel.id === message.author.dmChannel.id) {
            sendModmail(message);
        }
    }
}