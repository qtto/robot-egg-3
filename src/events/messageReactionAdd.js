const { Events } = require('discord.js');
const pinboard = require('../util/pinboard.js');

module.exports = {
    name: Events.MessageReactionAdd, 
    async execute(reaction, user){
        pinboard(reaction)
    }
}