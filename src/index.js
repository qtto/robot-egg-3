const { Client, Collection, GatewayIntentBits, Partials } = require('discord.js');
const { token } = require('../config.json');
const fs = require('fs');
const path = require('path');


// Create a new client instance
const client = new Client({ 
	intents: [GatewayIntentBits.Guilds, GatewayIntentBits.GuildMembers, GatewayIntentBits.GuildMessages, GatewayIntentBits.GuildMessageReactions, GatewayIntentBits.DirectMessages, GatewayIntentBits.MessageContent], 
	partials: [Partials.Message, Partials.Channel, Partials.Reaction] 
});


// Load commands
client.commands = new Collection();
const commandDir = path.resolve(__dirname, './commands');
const commandFiles = fs.readdirSync(commandDir).filter(file => file.endsWith('.js'));

// recognize commands
for (const file of commandFiles) {
	const command = require(`./commands/${file}`);
	client.commands.set(command.data.name, command);
	console.log("Loaded " + command.data.name);
}


// load events
const eventDir = path.resolve(__dirname, './events');
const eventFiles = fs.readdirSync(eventDir).filter(file => file.endsWith('.js'));

// handle events
for (const file of eventFiles) {
	const event = require(`./events/${file}`);
	if (event.once) {
		client.once(event.name, (...args) => event.execute(...args));
	} else {
		client.on(event.name, (...args) => event.execute(...args));
	}
}


// load tasks
const taskDir = path.resolve(__dirname, './backgroundTasks');
try {
	const taskFiles = fs.readdirSync(taskDir).filter(file => file.endsWith('.js'));

	// start tasks
	for (const file of taskFiles) {
		const task = require(`./backgroundTasks/${file}`);
		task.start(client);
	}

} catch (error) { console.error(error) }


// check if DB exists, else create it
const { dbFile } = require('./db-common.js');
const createDb = require('./util/strikes/create-db.js')
try {
	if (fs.existsSync(dbFile)) {
	  console.log(`Database ${dbFile} exists.`)
	} else {
		console.log(`Database ${dbFile} does not exist, creating...`)
		createDb(dbFile);
	}
  } catch(err) {
	console.error(err)
  }

// Login to Discord with token
client.login(token);