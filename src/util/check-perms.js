const { Client, Intents } = require('discord.js');
const { mainGuildId, ownerId, modRole, managerRole } = require('../../config.json');

//const client = new Client({ intents: [Intents.FLAGS.GUILDS] });

module.exports = (role, user_id, client, user_roles) => {
    //let guild = client.guilds.cache.get(guildId);
    //guild.commands.fetch()
    //.then(commands => commands.forEach(command => console.log(`${command.name}: ${command.id}`)))
    //.catch(console.error);

    if (user_id == ownerId) {return true};

    if(role === 'mod')  return user_roles.member._roles.includes(modRole);
    if(role === 'manager')  return user_roles.member._roles.includes(managerRole) || user_roles.member._roles.includes(modRole);
};