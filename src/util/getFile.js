const https = require('https')
const fs = require('fs');

module.exports = (url, filepath) => {
    const path = `${__dirname}/files/${filepath}`; 
    const fileStream = fs.createWriteStream(path);

    return new Promise((resolve, reject) => {
        const req = https.get(url, res => {
            res.pipe(filePath);

            fileStream.on('finish',() => {
                fileStream.close();
                resolve(filepath)
            })
        }).on('error', error => {
            reject(error.message);
        })    
        req.end()
    })
};