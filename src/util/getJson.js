const https = require('https')
const fs = require('fs');

module.exports = (host, path, headers = false) => {
    const options = {
    hostname: host,
    port: 443,
    path: path,
    method: 'GET'
    }

    if (headers) {
        options.headers = headers;
    }
    
    return new Promise((resolve, reject) => {
        const req = https.request(options, res => {
            let rawData = '';
            res.on('data', d => {
                rawData += d;
            })

            res.on('close', () => {
                try {
                    const parsedData = JSON.parse(rawData);
                    resolve(parsedData);
                } catch (error) {
                    reject(error.message);
                }
            })
        }).on('error', error => {
            reject(error.message);
        })    
        req.end()
    })
};