const { modmail } = require('../../config.json');

module.exports = message => {
    const client = message.client;
    const attachments = [];

    if(!message.content) message.content = '';

    if(message.attachments) {
        message.attachments.forEach( att =>
            attachments.push(att.url)
        )
    }

    client.channels.fetch(modmail)
    .then(channel => channel.send({
        content: `Message from ${message.author.username}:\n\n${message.content}`,
        files: attachments
        })
    )
}