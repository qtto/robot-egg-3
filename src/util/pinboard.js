const { pinboard } = require('../../config.json');
const { EmbedBuilder, AttachmentBuilder } = require('discord.js');

const minCount = 6;
const otherCount = 10;
const emoji = ['📌', '⭐', '🌟', '✨'];

global.messages = []

const checkPosted = (reaction, message) => {
    if (global.messages.length < 1) return false
    return global.messages.some(item => item.original == message.id);
}

async function updateMessage(reaction, message) {
    const client = message.client;
    // get id's of original message and pinned message and stuff
    let originalData = global.messages.filter(item => item.original == message.id)[0];

    // don't do anything if the new react is not the same as the one that got it pinned
    if (originalData.reacts._emoji != reaction._emoji) return

    // get the pinned message so we can edit
    let channel = await client.channels.fetch(pinboard);
    let pinnedMessage = await channel.messages.fetch(originalData.pin);

    // build new embed footer 
    let newEmbeds = pinnedMessage.embeds;
    if (reaction.emoji.url != null) { // it's a custom emoji so we need to set it as icon
        newEmbeds[0].data.footer = { text: `x${reaction.count}`, icon_url: `${reaction.emoji.url}` }
    } else
        newEmbeds[0].data.footer = { text: `${reaction.emoji}x${reaction.count}` }

    await pinnedMessage.edit(
        { embeds: newEmbeds }
    )
}

const buildEmbeds = (reaction, message) => {
    let embeds = [];
    let attachments = [];

    embed = new EmbedBuilder()
        .setColor('Random')
        .setAuthor({ name: `${message.author.username} | in #${message.channel.name}`, iconURL: message.author.avatarURL(), url: message.url })
        .setDescription(message.content ? message.content : null)
        .setTimestamp(message.createdTimestamp)

    if (message.attachments.size > 0) {
        if (message.attachments.size === 1 && message.attachments.first().contentType.startsWith("image")) {
            embed.setImage(message.attachments.first().url)
        }
        else {
            message.attachments.each(att => {
                let newatt = new AttachmentBuilder();
                newatt.setFile(att.url);
                newatt.setName(att.name);
                attachments.push(newatt);
            })
        }
    }


    if (reaction.emoji.url != null) { // it's a custom emoji so we need to set it as icon
        embed.setFooter({ text: `x${reaction.count}`, iconURL: `${reaction.emoji.url}` });
    } else
        embed.setFooter({ text: `${reaction.emoji}x${reaction.count}` });

    embeds.push(embed)

    if (message.embeds.length > 0) {
        embeds.push(message.embeds[0])
    }

    return [embeds, attachments]
}


module.exports = async (reaction) => {
    let message = reaction.message;
    const client = message.client;

    if (reaction.count === null) { // indicating it's out of cache
        message = await reaction.message.fetch();
        reaction = await reaction.fetch();
    }

    if (reaction.count < minCount || (reaction.count < otherCount && !emoji.includes(reaction.emoji.name)))
        // if it doesnt qualify (so we dont need to put everything in an if block lol)
        return

    if (checkPosted(reaction, message)) {
        await updateMessage(reaction, message);
        return
    }

    if (message.createdTimestamp < (Date.now() - 7 * 24 * 60 * 60 * 1000)) // more than a week ago
        return;

    client.channels.fetch(pinboard)
        .then(channel =>
            channel.send({ embeds: buildEmbeds(reaction, message)[0], files: buildEmbeds(reaction, message)[1] })
                .then(res => {
                    if (global.messages.length > 20) global.messages.shift()
                    global.messages.push({ 'pin': res.id, 'original': message.id, 'reacts': reaction })
                })
        )
}
