module.exports = async (user, message) => {
    return new Promise((resolve, reject) => {

        if(message.length > 1600) {
            reject(`Message too long (${message.length}/1600)`);
        };

        user.send(message)
        .then(res => {
            resolve(`Message sent to ${user}:\n\n${message}`);
        })
        .catch(err => { 
            console.error(err);
            reject(`Error sending message (${err}). Original message:\n\n${message}`);
        })
    })
}
