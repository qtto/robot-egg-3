const sqlite3 = require('sqlite3')
const {dbFile, closeDb} = require('../../db-common.js')

module.exports = () => {
    const db = new sqlite3.Database(dbFile, sqlite3.OPEN_READWRITE, (err) => {
        if (err && err.code == "SQLITE_CANTOPEN") {
            createDatabase();
            return;
            } else if (err) {
                console.log("Getting error " + err);
                return false;
            }
    });

    const createDatabase = () => {
        var newdb = new sqlite3.Database(dbFile, (err) => {
            if (err) {
                console.log("Error creating database " + err);
                return false;
            }
            createTables(newdb);
        });
    }

    const createTables= (newdb) => {
        newdb.exec(`
        CREATE TABLE strikes (
            id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
            user_id VARCHAR NOT NULL,
            tier_id INTEGER NOT NULL,
            reason VARCHAR(512) NOT NULL,
            attachment VARCHAR(512),
            created_on DATETIME NOT NULL,
            created_by VARCHAR NOT NULL,
            times_expired INTEGER NOT NULL DEFAULT 0,
            last_expired DATETIME,
            expired BOOLEAN DEFAULT 0,
            removed BOOLEAN DEFAULT 0,
            removed_by VARCHAR
        );

        CREATE TABLE mutes (
            points INTEGER NOT NULL,
            duration INTEGER NOT NULL
        );

        CREATE TABLE tiers (
            tier_id INTEGER PRIMARY KEY NOT NULL,
            name VARCHAR NOT NULL,
            points INTEGER NOT NULL,
            can_expire BOOLEAN NOT NULL
        );
    
        CREATE TABLE users (
            id VARCHAR PRIMARY KEY UNIQUE NOT NULL,
            next_expire DATETIME
        );
    
        INSERT INTO mutes (points, duration)
        VALUES  (1, 0),
                (2, 6),
                (3, 12),
                (4, 24),
                (5, 48),
                (6, 72),
                (7, 96),
                (8, 120),
                (9, 144),
                (10, 168),
                (11, 216);
        
        INSERT INTO tiers (tier_id, name, points, can_expire)
        VALUES  (1, 'green', 1, 1),
                (2, 'yellow', 2, 1),
                (3, 'orange', 4, 1),
                (4, 'red', 8, 1),
                (5, 'carmine', 12, 0);

        CREATE VIEW userView
        AS
        SELECT users.id,
            users.next_expire,
            (SUM(tiers.points)-SUM(strikes.times_expired)) AS points
        FROM users
            INNER JOIN strikes ON users.id = strikes.user_id
            INNER JOIN tiers ON strikes.tier_id = tiers.tier_id
        WHERE strikes.expired = 0 
            AND strikes.removed = 0
        GROUP BY users.id
        `
        );
    }
        
    closeDb(db);
};