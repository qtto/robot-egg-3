const {connectDb, closeDb} = require('../../db-common.js')

module.exports = (mod_id, strike_id) => {
    return new Promise((resolve, reject) => {
        let db = connectDb();

        db.run(`UPDATE strikes
                SET removed = 1,
                    removed_by = ${mod_id}
                WHERE id = ${strike_id}`, 
            (err) => {
                if (err) {
                    console.error(err.message);
                    reject(err);
                }
            }, () => resolve(true)
        )   
          
        closeDb(db);
    })
};