const {connectDb, closeDb} = require('../../db-common.js')
const { strikeDecayTime } = require('../../../config.json');

module.exports = (user_id, mod_id, reason, attachment, tier) => {
    return new Promise((resolve, reject) => {
        let db = connectDb();
        let now = new Date();
        let next_expire;

        const insertData = () => {
            return new Promise((resolve, reject) => {
                db.serialize(() => {
                    db.run(`INSERT INTO strikes (user_id, tier_id, reason, attachment, created_on, created_by)
                            VALUES ('${user_id}', ${tier}, '${reason}', '${attachment}', '${now.toISOString()}', '${mod_id}')`,
                    (err) => {
                        if (err) {
                            console.error(err.message);
                            reject(err);
                    }}),

                    // this bit is needed to calculate the new date for next_expire
                    db.get(`SELECT userView.points as user_points,
                            userView.next_expire as next_expire,
                            mutes.duration as mute
                            FROM userView
                            INNER JOIN mutes ON user_points = mutes.points OR mutes.points = 11
                            WHERE id = '${user_id}'`,
                    (err, row) => {
                        if (err) {
                            console.error(err.message);
                            reject(err);
                        }
                        if(row === undefined || row.next_expire == null) { 
                            // in case it wasn't set before, just set it to current time + 1 interval
                            next_expire = new Date(new Date().getTime()+(strikeDecayTime * 24 * 60 * 60 * 1000))
                        } else {
                            // if it was set before but all strikes decayed, base off current time, else add the mute to expire time and keep goin
                            let oldNextExpire = new Date(row.next_expire)
                            let baseExpireOn = (oldNextExpire < now) ? now : oldNextExpire
                            next_expire = new Date(new Date(baseExpireOn).getTime()+(row.mute * 60 * 60 * 1000));
                        }

                        db.run(`INSERT OR REPLACE INTO users (id, next_expire)
                            VALUES ('${user_id}', '${next_expire.toISOString()}')`,
                        (err) => {
                            if (err) {
                                console.error(err.message);
                                reject(err);
                            }
                            resolve(true)
                        })
                    })
                })
            })
        }

        const getUserData = () => {
            // return aantal points, mute tijd
            return new Promise((resolve, reject) => {
                db.get(`SELECT userView.points as user_points,
                            mutes.duration as mute
                            FROM userView
                            INNER JOIN mutes ON user_points = mutes.points OR mutes.points = 11
                            WHERE id = '${user_id}'`, 
                    (err, row) => {
                        if (err) {
                            console.error(err.message);
                            reject(err);
                        }
                        if (row.mute != null) {
                            let hours = row.mute;
                            let readable = '';

                            if (hours >= 24) {
                                let days = Math.floor(hours / 24)
                                hours = row.mute%24
                                readable += `${days} days`
                                readable += hours > 0 ? `, ${hours} hours.` : '.'
                            } else readable += `${hours} hours.`
                            row.mute = readable
                        }
                        resolve(row);
                    }
                );
            });
        }
        
        insertData()
        .then( res => {
            if(res) return getUserData()
            else reject("Could not enter data into database.")
        })
        .then( res => {
            closeDb(db);
            resolve(res);
        })
        .catch(err => {console.error(`Error creating strike: ${err}`)})
    })
};