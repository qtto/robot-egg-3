const {connectDb, closeDb} = require('../../db-common.js')
const { strikeDecayTime } = require('../../../config.json');

module.exports = () => {
    let db = connectDb();

    const getStrikes = () => {                
        const addExpireInterval = (d, times=1) => {
            return new Date(d.getFullYear(),d.getMonth(),d.getDate() + times * strikeDecayTime, d.getHours(), d.getMinutes());
        }

        const now = new Date();
        let strikes = [] // strikes to update in db

        return new Promise((resolve, reject) => {
            db.each(`SELECT strikes.id AS id,
                user_id,
                times_expired, 
                tiers.points as max_points,
                users.next_expire as next_expire,
                last_expired
                FROM strikes
                INNER JOIN tiers on strikes.tier_id = tiers.tier_id
                INNER JOIN users on strikes.user_id = users.id
                WHERE strikes.expired is 0 AND strikes.removed is 0 and tiers.can_expire is 1
                GROUP BY user_id`,
            (err, row) => {
                if (err) {
                    console.error(err.message);
                    reject(err);
                }
                if (!row) reject(false); // in case no valid strikes to expire
                if (new Date(row.next_expire) > now) return; // in case expiry date not reached

                let s = {} // object to put new strike data in
                s.nextExpire = new Date(row.next_expire);
    
                // Fallback in case db is out of date - expire as many times as necessary
                let dateDiff = Math.round((now - s.nextExpire) / (1000 * 60 * 60 * 24)); // difference in days since nextExpire
                let expireTimes = Math.floor(dateDiff / strikeDecayTime) + 1; // times strike should expire, starting at 1 with extra for every interval on top

                // put data in new strike. Calc next expiry date based on when it should've expired last
                s.lastExpired = now;
                s.timesExpired = Math.min(row.times_expired + expireTimes, row.max_points) //don't expire more than possible
                s.nextExpire = addExpireInterval(s.nextExpire, expireTimes);
                s.expired = s.timesExpired >= row.max_points;
                s.user_id = row.user_id;
                s.id = row.id;

                strikes.push(s);
            }, () => resolve(strikes));
        })
    }

    const updateDatabase = (strikes) => {
        let amount = 0;
        return new Promise((resolve, reject) => {
            strikes.forEach(s => {
                db.parallelize(()=>{
                    db.run(`UPDATE strikes
                    SET times_expired = ${s.timesExpired}, 
                    last_expired = '${s.lastExpired.toISOString()}', 
                    expired = ${+ s.expired}
                    WHERE id = ${s.id}`, 
                    (err) => { 
                        if (err){
                            console.error(err.message); 
                            reject(err);
                        }
                    });
                    
                    db.run(`UPDATE users
                    SET next_expire = '${s.nextExpire.toISOString()}'
                    WHERE id = ${s.user_id}`, 
                    (err) => { 
                        if (err){
                            console.error(err.message); 
                            reject(err);
                        }
                    });
                })
                amount++;
            });
            resolve(amount);
        })
    }

    // return amount of strikes decayed
    return new Promise((resolve, reject) => {
        getStrikes()
        .then(strikes => {
            return updateDatabase(strikes)}
        )
        .then(res => {
            closeDb(db);
            resolve(res);
        })
        .catch(err => {console.log(`Error in updating strike database: ${err}`)})
    })
};