const {connectDb, closeDb} = require('../../db-common.js')

module.exports = user_id => {
    return new Promise((resolve, reject) => {
        let db = connectDb()

        const getUserStrikes = (db, user_id) => {
            return new Promise((resolve, reject) => {
                let strikes = [];
                db.each(`SELECT id,
                            user_id,
                            tier_id as tier,
                            reason,
                            removed,
                            expired
                            FROM strikes
                            WHERE user_id = ${user_id}`, 
                    (err, row) => {
                        if (err) {
                            console.error(err.message);
                            reject(err);
                        }
        
                        strikes.push(row);
                    }, () => resolve(strikes)            
                );
            });
        }        
        
        const getUserPoints = (db, user_id) => {
            return new Promise((resolve, reject) => {
                db.get(`SELECT points
                            FROM userView
                            WHERE id = ${user_id}`, 
                    (err, row) => {
                        if (err) {
                            console.error(err.message);
                            reject(err);
                        }
                        resolve(row != undefined ? row.points : 0);
                    }
                );
            });
        }

        const result = Promise.all(
            [getUserStrikes(db, user_id),
            getUserPoints(db, user_id)]
        ) 

        result.then(
            res => resolve({'strikes': res[0], 'strike_points': res[1]})
        )
          
        closeDb(db);
    })
};